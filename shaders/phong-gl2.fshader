uniform vec3 uLight, uLight2, uColor;

varying vec3 vNormal;
varying vec3 vPosition;

void main() {
  vec3 toV = -normalize(vec3(vPosition));
  vec3 toLight = uLight - vec3(vPosition);
  vec3 toLight2 = uLight2 - vec3(vPosition);
  toLight = normalize(toLight);
  toLight2 = normalize(toLight2);
  vec3 h1 = normalize(toV + toLight);
  vec3 h2 = normalize(toV + toLight2);

  vec3 color = vec3(1.0, 0.7, 0.1);
  vec3 normal = normalize(vNormal);

  if(gl_FrontFacing) color = uColor;
  if(!gl_FrontFacing) normal = -normal;  
 
  float diffuse = max(0.0, dot(normal, toLight));
  diffuse += max(0.0, dot(normal, toLight2));
  float specular = pow(max(0.0, dot(h1, normal)), 64.0);
  specular += pow(max(0.0, dot(h2, normal)), 64.0);
  vec3 intensity =  vec3(0.1,0.1,0.1) + uColor * diffuse + vec3(0.6, 0.6, 0.6) * specular;
 
  gl_FragColor = vec4(intensity.x, intensity.y, intensity.z, 1.0);
}
