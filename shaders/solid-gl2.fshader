uniform vec3 uColor;

void main() {
  if (gl_FrontFacing) gl_FragColor = vec4(uColor, 1.0);
  else gl_FragColor = vec4(1.0,0.7,0.1, 1.0);
}
